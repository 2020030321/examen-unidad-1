
package Modelo;


public class Docente {
    
    private int numDocente, nivel, horasImpartidas;
    private String nombre, domicilio;
    private float pagoBase;

    public Docente(int numDocente, int nivel, int horasImpartidas, String nombre, String domicilio, float pagoBase) {
        this.numDocente = numDocente;
        this.nivel = nivel;
        this.horasImpartidas = horasImpartidas;
        this.nombre = nombre;
        this.domicilio = domicilio;
        this.pagoBase = pagoBase;
    }
    
    public Docente(Docente ob) {
        this.numDocente = ob.numDocente;
        this.nivel = ob.nivel;
        this.horasImpartidas = ob.horasImpartidas;
        this.nombre = ob.nombre;
        this.domicilio = ob.domicilio;
        this.pagoBase = ob.pagoBase;
        
    }
    
    public Docente() {
        this.numDocente = 0;
        this.nivel = 0;
        this.horasImpartidas = 0;
        this.nombre = "";
        this.domicilio = "";
        this.pagoBase = 0;
    }
    
    //GETS
    public void setNumDocente(int numDocente) {
        this.numDocente = numDocente;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public void setHorasImpartidas(int horasImpartidas) {
        this.horasImpartidas = horasImpartidas;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public void setPagoBase(float pagoBase) {
        this.pagoBase = pagoBase;
    }
    
    //GETS
    public int getNumDocente() {
        return numDocente;
    }

    public int getNivel() {
        return nivel;
    }

    public int getHorasImpartidas() {
        return horasImpartidas;
    }

    public String getNombre() {
        return nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public float getPagoBase() {
        return pagoBase;
    }
    
    
    
    public float calcularPago(){
       /* return switch (nivel) {
            case 1 -> pagoBase + (pagoBase*0.30f)*horasImpartidas;
            case 2 -> pagoBase + (pagoBase*0.50f)*horasImpartidas;
            case 3 -> pagoBase + (pagoBase*1)*horasImpartidas;
            default -> -1;
        }; */
        
        switch (nivel){
            case 1:
                return (pagoBase+(pagoBase*0.30f))*horasImpartidas;
            case 2:
                return (pagoBase+(pagoBase*0.50f))*horasImpartidas;
            case 3:
                return (pagoBase+(pagoBase*1))*horasImpartidas;
            default:
                return -1;
                
        }
        
    }
    
    public float calcularImpuestos(){
        return (calcularPago()*0.16f);
    }
    
    public float calcularBono(int hijos){
        float bono = 0.0f;
        if (hijos<=0){
            bono = 0;
        }
        else if(hijos>=1 && hijos<=2){
            bono = ((calcularPago()*0.05f));
        } 
        else if(hijos>=3 && hijos<=5){
            bono = ((calcularPago()*0.10f));
        }
        else if (hijos>5){
            bono = ((calcularPago()*0.20f));
        }
        return bono;
        
    }
    
    
}
