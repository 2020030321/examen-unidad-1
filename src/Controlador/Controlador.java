

package Controlador;

import Modelo.Docente;
import Vista.dlgVista;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;


public class Controlador implements ActionListener{

    Docente doc;
    dlgVista vista;

    public Controlador(Docente doc, dlgVista vista) {
        this.doc = doc;
        this.vista = vista;
        
        vista.btnNuevo.addActionListener(this);
        vista.btnCancelar.addActionListener(this);
        vista.btnCerrar.addActionListener(this);
        vista.btnGuarda.addActionListener(this);
        vista.btnLimpiar.addActionListener(this);
        vista.btnMostrar.addActionListener(this);
        
        
    }
    
    public void iniciarVista(){
        vista.setTitle("PAGO A DOCENTE");
        vista.setSize(800, 800);
        vista.setVisible(true);
        
    }
    
    
    public static void main(String[] args) {
        
        dlgVista vista = new dlgVista(new JFrame(), true);
        Docente doc = new Docente();
        Controlador con = new Controlador(doc, vista);
        con.iniciarVista();        
        
    }

    public void apagar(){
        vista.txtDomicilio.setEnabled(false);
        vista.txtNombre.setEnabled(false);
        vista.txtNumDocente.setEnabled(false);
        vista.txtPagoPorHoraBase.setEnabled(false);
        vista.txtHorasImpartidas.setEnabled(false);
        vista.cboNivel.setEnabled(false);
        
        
    }
    
    public void limpiar(){
            vista.txtNombre.setText("");
            vista.txtDomicilio.setText("");
            vista.txtPagoPorHoraBase.setText("");
            vista.txtHorasImpartidas.setText("");
            vista.txtNumDocente.setText("");
            vista.cboNivel.setSelectedIndex(0);
            vista.lblDescuentoPorImpuesto.setText("0");
            vista.lblPagoPorBono.setText("0");
            vista.lblPagoPorHoraImpartida.setText("0");
            vista.lblTotalPago.setText("0");
            
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        
        if(e.getSource()==vista.btnNuevo){
            
            vista.txtNumDocente.setEnabled(true);
            vista.txtNombre.setEnabled(true);
            vista.txtDomicilio.setEnabled(true);
            vista.cboNivel.setEnabled(true);
            vista.txtPagoPorHoraBase.setEnabled(true);
            vista.txtHorasImpartidas.setEnabled(true);
            
            vista.btnGuarda.setEnabled(true);
            
            vista.spnHijos.setEnabled(true);
            
            
            limpiar();
           

        }
        
        if(e.getSource()==vista.btnGuarda){
            
            try{
                doc.setNumDocente(Integer.parseInt(vista.txtNumDocente.getText()));
                doc.setNombre(vista.txtNombre.getText());
                doc.setDomicilio(vista.txtDomicilio.getText());
                doc.setPagoBase(Float.parseFloat(vista.txtPagoPorHoraBase.getText()));
                doc.setHorasImpartidas(Integer.parseInt(vista.txtHorasImpartidas.getText()));

            }
            catch(NumberFormatException ex){
                JOptionPane.showMessageDialog(vista, "Surgió el siguiente error: " + ex.getMessage());
                return;
            }
            catch(Exception ex2){
                
                JOptionPane.showMessageDialog(vista, "Surgió el siguiente error: " + ex2.getMessage());
                return;
            }
            try{
                
                if(vista.cboNivel.getSelectedItem()=="Licenciatura o Ingenieria")
                    doc.setNivel(1);
                else if(vista.cboNivel.getSelectedItem()=="Maestro en Ciencias")
                    doc.setNivel(2);
                else if(vista.cboNivel.getSelectedItem()=="Doctor")
                    doc.setNivel(3);
            }
            catch(Exception ex3){
                
                JOptionPane.showMessageDialog(vista, "Surgió el siguiente error: " + ex3.getMessage());
                return;
            }
            
            vista.btnMostrar.setEnabled(true);
            
        }
        
        if(e.getSource()==vista.btnMostrar){
            
            vista.txtDomicilio.setText(String.valueOf(doc.getNumDocente()));
            vista.txtNombre.setText(doc.getNombre());
            vista.txtDomicilio.setText(doc.getDomicilio());
            vista.txtPagoPorHoraBase.setText(String.valueOf(doc.getPagoBase()));
            vista.txtHorasImpartidas.setText(String.valueOf(doc.getHorasImpartidas()));
            
            if(doc.getNivel()==1){
                vista.cboNivel.setSelectedIndex(1);
            }
            else if (doc.getNivel()==2){
                vista.cboNivel.setSelectedIndex(2);
            }
            else if (doc.getNivel()==3){
                vista.cboNivel.setSelectedIndex(3);
            }
            
            vista.lblPagoPorHoraImpartida.setText(String.valueOf(doc.calcularPago()));
            vista.lblPagoPorBono.setText(String.valueOf(doc.calcularBono(Integer.parseInt(vista.spnHijos.getValue().toString()))));
            vista.lblDescuentoPorImpuesto.setText(String.valueOf(doc.calcularImpuestos()));
            float total;
            total = (doc.calcularPago()-doc.calcularImpuestos()+doc.calcularBono(Integer.parseInt(vista.spnHijos.getValue().toString())));
            vista.lblTotalPago.setText(String.valueOf(total));
            
            vista.btnLimpiar.setEnabled(true);
            vista.btnCancelar.setEnabled(true);
        }
        
        if (e.getSource()==vista.btnLimpiar){
            limpiar();
            
        }
        
        if (e.getSource()==vista.btnCancelar){
            limpiar();
            apagar();
            
        }
        
        if(e.getSource()==vista.btnCerrar){
                int option = JOptionPane.showConfirmDialog(vista, "¿Desea salir?",
                        "Seleccione", JOptionPane.YES_NO_OPTION);
                if(option == JOptionPane.YES_NO_OPTION){
                    vista.dispose();
                    System.exit(0);
            }
        }
        
    }
    
}
